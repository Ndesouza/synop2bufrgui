import os
import os.path
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QDialog, QMessageBox
from PyQt5.QtCore import QLocale, QTranslator
from ui_mainwindow import Ui_MainWindow

class MainWindow(QMainWindow):

    userPath = os.path.expanduser("~")
    configFile = os.path.join(userPath, ".config/synop2bufrgui.conf")
    tempSynopFile = os.path.join(userPath, ".config/tempsynop")
    tempBufrFile = os.path.join(userPath, ".config/tempbufr")
    blank = "\r\n"
    colorGreen = "background-color: green"
    colorRed = "background-color: red"

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # Set up user interface
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Connect buttons
        self.ui.synopPB.clicked.connect(self.synop2BufrFilePath)
        self.ui.outputPB.clicked.connect(self.outputFilePath)
        self.ui.inputPB.clicked.connect(self.inputFile)
        self.ui.convertPB.clicked.connect(self.onExecute)
        self.ui.actionExit.triggered.connect(self.close)

        # Read config file
        if os.path.exists(self.configFile):
            self.readConfig()

        self.enableConversion()

    def readConfig(self):
        if os.path.isfile(self.configFile):
            config = open(self.configFile, 'r')
            self.ui.synopLE.setText(config.readline().rstrip("\n"))
            self.ui.outputLE.setText(config.readline().rstrip("\n"))
            config.close()

    def writeConfig(self):
        config = open(self.configFile, 'w')
        config.write(self.ui.synopLE.text() + "\n")
        config.write(self.ui.outputLE.text() + "\n")
        config.close()

    def synop2BufrFilePath(self):

        folderPath = QFileDialog.getExistingDirectory(self, self.tr("Select the Synop2Bufr Path"))

        if(self.isValidSynop2BufrPath(folderPath)):
            self.ui.synopLE.setText(folderPath)
            self.ui.synopLE.setStyleSheet(self.colorGreen)
        else:
            self.ui.synopLE.setText(self.tr("Invalid Path"))
            self.ui.synopLE.setStyleSheet(self.colorRed)

        self.enableConversion()

    def outputFilePath(self):
        outputFilePath = QFileDialog.getExistingDirectory(self, self.tr("Output File path"))
        self.ui.outputLE.setText(outputFilePath)
        self.enableConversion()

    def inputFile(self):

        inputFile = QFileDialog.getOpenFileName(self, self.tr("Select Synop File"))

        if(self.isValidSynopFile(inputFile[0])):
            self.ui.inputLE.setText(inputFile[0])
            self.ui.inputLE.setStyleSheet(self.colorGreen)
        else:
            self.ui.inputLE.setText(self.tr("Invalid Synop File"))
            self.ui.inputLE.setStyleSheet(self.colorRed)

        self.enableConversion()

    def isValidSynopFile(self, synopFile = ""):
        if not synopFile:
            return False    
        else:
            synop = open(synopFile, 'r')
            line = synop.readline()
            if( str(line).startswith(("SM", "SI")) ):
                return True
            else:
                return False

    def isValidSynop2BufrPath(self, path):
        if(os.path.isfile(os.path.join(path, "synop2bufr.o"))):
            return True
        else:
            return False

    def formatSynop(self):
        if not os.path.exists(os.path.join(self.userPath, ".config")):
            try:
                os.mkdir(os.path.join(self.userPath, ".config"))
            except OSError:
                print(self.tr("Could not create directory"))

        tempfile = open(self.tempSynopFile, 'wb')
        tempfile.write("\u0001\r".encode('ascii'))  #Write start of heading
        tempfile.write(self.blank.encode('ascii'))
        tempfile.write("001\r".encode('ascii'))
        tempfile.write(self.blank.encode('ascii'))

        synop = open(self.ui.inputLE.text(), 'r')
        for line in synop:
            if line == '\n':
                tempfile.write(self.blank.encode('ascii'))
            else:
                tempfile.write((line.rstrip("\r\n") + '\r').encode('ascii'))

        tempfile.write(self.blank.encode('ascii'))
        tempfile.write('\u0003'.encode('ascii'))    #Write end of transmission

        tempfile.close()
        synop.close()

    def formatBufr(self):

        synop = open(self.ui.inputLE.text(), 'r')
        line = synop.readline()
        par = line.split(" ")
        tempbufr = open(self.tempBufrFile, 'rb')
        outputBufrFilename = par[1] + "_" + par[2].rstrip("\n") + ".bufr"
        bufr = open(os.path.join(self.ui.outputLE.text(), outputBufrFilename), 'wb')
        filesize = os.path.getsize(self.tempBufrFile) + 19 # 19 is the amount of bytes added to the BUFR file 

        bufr.write(("****" + str(filesize).zfill(10) + "****\r").encode('ascii'))
        bufr.write(self.blank.encode('ascii'))
        if par[0].startswith("SI"):
            bufr.write(("ISIE20 " + par[1] + " " + par[2].rstrip("\n") + "\r").encode('ascii'))
        else:
            bufr.write(("ISME01 " + par[1] + " " + par[2].rstrip("\n") + "\r").encode('ascii'))
        for line in tempbufr:
            bufr.write(line)

        tempbufr.close()
        bufr.close()

    def onExecute(self):
        self.formatSynop()
        os.chdir(self.ui.synopLE.text())
        process = subprocess.Popen([os.path.join(self.ui.synopLE.text(), "synop2bufr"),
                                    "-i", self.tempSynopFile,
                                    "-o", self.tempBufrFile,
                                    "-c", "-87"],
                                    stdout=subprocess.PIPE)
        output, err = process.communicate()
        self.ui.outputTE.append(str(output, encoding='UTF-8'))
        
        self.formatBufr()

    def enableConversion(self):
        if(self.isValidSynop2BufrPath(self.ui.synopLE.text()) and
            self.isValidSynopFile(self.ui.inputLE.text()) and
            os.path.lexists(self.ui.outputLE.text())):

            self.ui.convertPB.setEnabled(True)
            self.writeConfig()
        else:
            self.ui.convertPB.setEnabled(False)
       
if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    # Internationalization
    locale = QLocale.system().name()
    print(locale)
    qtTranslator = QTranslator()
    if qtTranslator.load("i8ln/lang_%s.qm" % locale):
        app.installTranslator(qtTranslator)

    window = MainWindow()
    window.show()
    sys.exit(app.exec_())