<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Synop2Bufr GUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="37"/>
        <source>Convert</source>
        <translation type="unfinished">Convertir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <source>Paths</source>
        <translation type="unfinished">Ruta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="66"/>
        <source>Output Path</source>
        <translation type="unfinished">Ruta de Destino</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>Browse</source>
        <translation type="unfinished">Navegar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="100"/>
        <source>Synop2Bufr Path</source>
        <translation type="unfinished">Ruta para Destino</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="107"/>
        <source>Input File</source>
        <translation type="unfinished">Archivo Fuente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Output</source>
        <translation type="unfinished">Destino</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="139"/>
        <source>File</source>
        <translation type="unfinished">Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="145"/>
        <source>Help</source>
        <translation type="unfinished">Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <source>About</source>
        <translation type="unfinished">Acerca</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <source>Exit</source>
        <translation type="unfinished">Salir</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="53"/>
        <source>Select the Synop2Bufr Path</source>
        <translation type="unfinished">Seleccione la Ruta para Synop2Bufr</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="59"/>
        <source>Invalid Path</source>
        <translation type="unfinished">Ruta Incorrecta</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="65"/>
        <source>Output File path</source>
        <translation type="unfinished">Ruta de Archivo de Destino</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="71"/>
        <source>Select Synop File</source>
        <translation type="unfinished">Seleccione Archivo Synop</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="77"/>
        <source>Invalid Synop File</source>
        <translation type="unfinished">Archivo Synop Invalido</translation>
    </message>
    <message>
        <location filename="synop2bufrgui.py" line="104"/>
        <source>Could not create directory</source>
        <translation type="unfinished">No se puede crear Directorio</translation>
    </message>
</context>
</TS>
